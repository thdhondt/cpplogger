#pragma once
#include <string>
#include <map>
#include <fstream>
#include <vector>
#include <ctime>
#include <pthread.h>
#include <sstream>

namespace Logging {
    // ---------- Logging levels ----------
    enum Level {
        DEBUG   = 0,
        INFO    = 1,
        WARNING = 2,
        ERROR   = 3,
        FATAL   = 4
    };

    /**
     *  Logging record
     */
    class LogRecord{
        public:
            std::string message;
            Level       level;
            time_t      time;
    };

    /**
     *  Abstract formatter
     */
    class AbstractFormatter {
        public:
            virtual std::string format(const LogRecord& record)=0;

            // Mapping to string for the logger level
            const std::map<Level, std::string> StringMap = {
                {Level::DEBUG,   "Debug"},
                {Level::INFO,    "Info"},
                {Level::WARNING, "Warning"},
                {Level::ERROR,   "Error"},
                {Level::FATAL,   "Fatal"}
            };
    };

    /** */
    class ColorFormatter:public AbstractFormatter {
        public:
            std::string format(const LogRecord& record);

        private:
            // Color definitions
            enum class Colors : unsigned char {
                DEFAULT = 39,
                BLACK   = 30,
                RED     = 31,
                GREEN   = 32,
                YELLOW  = 33,
                BLUE    = 34,
                MAGENTA = 35,
                CYAN    = 36,
                GRAY    = 37
            };

            // Color mapping for the logger level
            const std::map<Level, Colors> ColorMap = {
                {Level::DEBUG,   Colors::GREEN},
                {Level::INFO,    Colors::BLUE},
                {Level::WARNING, Colors::YELLOW},
                {Level::ERROR,   Colors::RED},
                {Level::FATAL,   Colors::MAGENTA}
            };

            // Transform color in color code
            std::string getColorCode(Colors c);
    };

    class Formatter:public AbstractFormatter {
        public:
            std::string format(const LogRecord& record);
    };


    // ---------- Handlers -----------
    class AbstractHandler{
        public:
            AbstractHandler(AbstractFormatter &fmt);
            virtual void write(LogRecord record)=0;
            AbstractFormatter& _fmt;
    };

    class ConsoleHandler: public AbstractHandler{
        public:
            ConsoleHandler(AbstractFormatter &fmt): AbstractHandler(fmt) {}
            void write(LogRecord record);
    };

    class FileHandler: public AbstractHandler{
        public:
            FileHandler(const std::string &filepath, 
                        AbstractFormatter &fmt,
                        const std::string &extension = "log");
            ~FileHandler();
            void write(LogRecord record);
        private:
            // Log file handle
            std::ofstream _logfile;
    };

    class RotatingFileHandler: public AbstractHandler{
        public:
            RotatingFileHandler(const std::string &basefilepath, 
                                AbstractFormatter &fmt,
                                const std::string &extension = "log",
                                int n_files = 2,
                                int n_records = 1000);
            ~RotatingFileHandler();
            void write(LogRecord record);
        private:
            // Private methods
            std::string _get_filename();

            // Log file handle
            std::ofstream _logfile;

            // File naming
            std::string _basefilepath;
            std::string _extension;

            // Keep track of the state of the rotation
            int _n_files;
            int _n_records;
            int _records_cnt;
            int _file_cnt;
    };

    // ---------- Logger -----------
    class Log {
        public:
            // Constructor
            Log(Level l = DEBUG);
            ~Log();

            // Log a new message
            void log(Level s, const std::string& msg);
            void log(Level s, std::stringstream& msg);
            void log(Level s, std::ostream& msg);

            // Add handler
            void addHandler(AbstractHandler& handler);

        private:
            // Mutex for thread safe logging
            pthread_mutex_t _lock;

            // Vector of handlers
            std::vector<AbstractHandler*> _pHandlers;

            // Current time
            time_t _time;

            // Internal logging record
            LogRecord _record;

            // Logging level threshold
            const Level _l;
    };

    // Function used to grab a reference to the root logger
    Log* getLogger();
}