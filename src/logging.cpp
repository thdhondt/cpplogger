#include "logging.hpp"
#include <iostream>
#include <sstream>

namespace Logging {
    // --------- Global root logger ---------
    static Log* pLogger = nullptr;

    // ---------- Formatters -----------
    std::string ColorFormatter::format(const LogRecord& record){
        // Transform time in a readable format
        struct tm * timeinfo;
        char tbuffer [80];
        timeinfo = localtime (&record.time);
        strftime(tbuffer,80,"%c",timeinfo);

        // Format the log message
        std::ostringstream formated_stream;
        formated_stream << getColorCode(ColorMap.at(record.level)) 
                        << "[" << tbuffer << "] "
                        << "[" << StringMap.at(record.level) << "] "
                        << record.message 
                        << getColorCode(Colors::DEFAULT);

        // Return string
        return formated_stream.str();
    }

    std::string ColorFormatter::getColorCode(Colors c){
        std::string color_code = "\e[";
        color_code += std::to_string(static_cast<int>(c));
        color_code += "m";
        return color_code;
    }

    std::string Formatter::format(const LogRecord& record){
        // Transform time in a readable format
        struct tm * timeinfo;
        char tbuffer [80];
        timeinfo = localtime (&record.time);
        strftime(tbuffer,80,"%c",timeinfo);

        // Format the log message
        std::ostringstream formated_stream;
        formated_stream << "[" << tbuffer << "] "
                        << "[" << StringMap.at(record.level) << "] "
                        << record.message;

        // Return string
        return formated_stream.str();
    }

    // ---------- Handlers -----------
    AbstractHandler::AbstractHandler(AbstractFormatter &fmt): _fmt(fmt){}

    void ConsoleHandler::write(LogRecord record){
        std::cout << _fmt.format(record) << std::endl;
    }

    FileHandler::FileHandler(const std::string &filepath, 
                             AbstractFormatter &fmt,
                             const std::string &extension):
    AbstractHandler(fmt){
        _logfile.open(filepath + "." + extension);
    }
    
    FileHandler::~FileHandler(){
        _logfile.close();
    }

    void FileHandler::write(LogRecord record){
        if (_logfile.is_open()) {
            _logfile << _fmt.format(record) << std::endl;
        }
    }

    RotatingFileHandler::RotatingFileHandler(const std::string &basefilepath, 
                                             AbstractFormatter &fmt,
                                             const std::string &extension,
                                             int n_files,
                                             int n_records):
    AbstractHandler(fmt),
    _basefilepath(basefilepath),
    _extension(extension),
    _n_files(n_files),
    _n_records(n_records),
    _records_cnt(0),
    _file_cnt(0)
    {
        // Check user input
        if (_n_files <= 0)
            std::cout << "Please specify a number of log files greater than 0" << std::endl;
        if (_n_records <= 0)
            std::cout << "Please specify a number of records per file greater than 0" << std::endl;

        // Create the first log file
        _logfile.open(_get_filename());
    }

    std::string RotatingFileHandler::_get_filename(){
        return _basefilepath + "_" + std::to_string(_file_cnt) + "." + _extension;
    }

    RotatingFileHandler::~RotatingFileHandler(){
        _logfile.close();
    }

    void RotatingFileHandler::write(LogRecord record){
        if (_logfile.is_open() && _records_cnt < _n_records) {
            // Log message
            _logfile << _fmt.format(record) << std::endl;

            // Increment counter
            _records_cnt ++;
        }
        else{
            // Increment file counter and wrap around at max files
            _file_cnt ++;
            _file_cnt = _file_cnt % _n_files;

            // Close file and open new one
            _logfile.close();
            _logfile.open(_get_filename());

            // Reset record count
            _records_cnt = 0;
        }


    }

    // ---------- Logger -----------
    Log::Log(Level l):
    _l(l){
        // Initialize mutex
        pthread_mutex_init(&_lock, NULL);

        // Overwrite root logger
        pLogger = this;
    }

    Log::~Log(){
        pthread_mutex_destroy(&_lock);
    }

    void Log::log(Level s, std::stringstream& msg) {
        // Do a regular call of the logging function
        log(s, msg.str());

        // Clean the stream
        msg.str(std::string()); 
    }

    void Log::log(Level s, std::ostream& msg){
        // Transform the ostream to a stringstream
        std::stringstream ss;
        ss << msg.rdbuf();

        // Do a regular call of the logging function
        log(s, ss.str());

    }

    void Log::log(Level s, const std::string& msg) {
        // Lock the logger
        pthread_mutex_lock(&_lock);

        // Check the logging level
        if (s >= _l) {
            // Get message time
            time(&_time);

            // Check if the logger is initialized
            if (pLogger == nullptr){
                std::cout << "Logger has not been initialized yet ..." << std::endl;
            }
            else{
                // Generate the record
                _record.level   = s;
                _record.message = msg;
                _record.time    = _time;

                // Forward record to all handlers
                for(auto handler : _pHandlers)
                {
                    handler->write(_record);
                }
            }
        }
        // Unlock the thread
        pthread_mutex_unlock (&_lock);
    }

    void Log::addHandler(AbstractHandler& handler){
        _pHandlers.push_back(&handler);
    }

    Log* getLogger(){
        return pLogger;
    }
}